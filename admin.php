<?php
    session_start();
    
    if(!isset($_SESSION['loggedIn'])){
        $_SESSION['loggedIn']=false;
    }
    
    if($_SESSION['loggedIn']==false){
        header("Location:login.php");
    }
?>


<!DOCTYPE html>
<html>
    <title>NASA-ADMIN</title>
    
    <link rel="stylesheet" type="text/css" href="nasa_style.css">
    
    <body>
        <div class="navbar">
            <a class="navbar_logo" href="index.php"></a>
            <div class="search">
                <form method="post" action="<?php echo $_SERVER['PHP_SELF'];?>" >
                    <input type="submit" name="logout" value="logout">
                    <?php                        
                        if(isset($_POST['logout'])){
                            session_destroy();
                            header('Refresh:0');
                        }
                    ?>
                </form>
            </div>
            <h1> ADMIN</h1>
        </div>
        <form action="upload.php" method="POST" enctype="multipart/form-data">
            <div class="uploads">
                Select Image for NewsImage:<br>
                <input type="file" name="fileForIMAGE" id="fileToUpload">
                <input type="textForImage" name="titleForImage" placeholder="Titel"><br><br>
                <textarea name="textForImage" rows="5" cols="20" placeholder="Article"></textarea>
                <input type="submit" name="imgUpload" value="Upload Image"><br><br>
            </div>
        </form>

        <form action="upload.php" method="POST" enctype="multipart/form-data">
            <div class="uploads">
                Select Image for Banner:<br>
                <input type="file" name="fileForBanner" id="fileToUpload"><br><br>
                <input type="text" name="titleForBanner" placeholder="Titel"><br><br>
                <input type="text" name="textForBanner" placeholder="Text">
                <input type="submit" name="bannerUpload" value="Upload Image"><br><br>
            </div>
        </form>
        
    </body>
</html>
