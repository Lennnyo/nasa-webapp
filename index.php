<!DOCTYPE html>
<html>
    <title>NASA</title>
    <link rel="stylesheet" type="text/css" href="nasa_style.css">
    <link rel="stylesheet" href"https://www.w3schools.com/w3css/4/w3.css">

    <style>
    .mySlides{display:none;}
    .fade {
        -webkit-animation-name: fade;
        -webkit-animation-duration: 1.5s;
        animation-name: fade;
        animation-duration: 1.5s;
    }

    @-webkit-keyframes fade {
        from {width: 0} 
        to {width: 100%}
    }

    @keyframes fade {
        from {width: 0} 
        to {width: 100%}
    }

    </style>
    <body> 
       
       <div class="navbar">
            <a class="navbar_logo" href="index.php"></a>
            <div class="search">
                <input type="text" placeholder="Search" name="search">
                <button type="submit">Q</button>
            </div>
            <div class="login">
                <form action="admin.php">
                    <input type="submit" value="login">
                </form>
           </div>
           <div class="main_menu">
                <li>
                    <div class="dropdown">
                        <a class="dropbtn">Missions</a>
                        <div class="dropdown-content">
                            <a href="news.php"> Newsarticle </a>
                            <a> adf </a>
                            <a> adf </a>
                            <a> adf </a>
                        </div>
                    </div>
                </li>
                
                <li>
                    <div class="dropdown">
                        <a class="dropbtn">Galeries</a>
                        <div class="dropdown-content">
                            <a> adf </a>
                            <a> adf </a>
                            <a> adf </a>
                            <a> adf </a>
                        </div>
                    </div>
                </li>
                
                <li>
                    <div class="dropdown">
                        <a class="dropbtn">NASA TV</a>
                        <div class="dropdown-content">
                            <a> adf </a>
                            <a> adf </a>
                            <a> adf </a>
                            <a> adf </a>
                        </div>
                    </div>
                </li>
                
                <li>
                    <div class="dropdown">
                        <a class="dropbtn">Follow NASA</a>
                        <div class="dropdown-content">
                            <a> adf </a>
                            <a> adf </a>
                            <a> adf </a>
                            <a> adf </a>
                        </div>
                    </div>
                </li>
                
                <li>
                    <div class="dropdown">
                        <a class="dropbtn">Downloads</a>
                        <div class="dropdown-content">
                            <a> adf </a>
                            <a> adf </a>
                            <a> adf </a>
                            <a> adf </a>
                        </div>
                    </div>
                </li>
                
                <li>
                    <div class="dropdown">
                        <a class="dropbtn">About</a>
                        <div class="dropdown-content">
                            <a> adf </a>
                            <a> adf </a>
                            <a> adf </a>
                            <a> adf </a>
                        </div>
                    </div>
                </li>
                
                <li>
                    <div class="dropdown">
                        <a class="dropbtn">NASA Audiences</a>
                        <div class="dropdown-content">
                            <a> adf </a>
                            <a> adf </a>
                            <a> adf </a>
                            <a> adf </a>
                        </div>
                    </div>
                </li>
                
            </div>
            <div class="sub_menu">
                <li><a>Humans in Space</a></li>
                <li><a>Moon to Mars</a></li>
                <li><a>Earth</a></li>
                <li><a>Space Tech</a></li>
                <li><a>Flight</a></li>
                <li><a>Solar System and beyond</a></li>
                <li><a>Education</a></li>
                <li><a>History</a></li>
                <li><a>Benifits to you</a></li>
            </div>
        </div>     
        
        
        <div class="grid-container">
        
        
        <?php

//
//            echo "<img".$img_arr[1]."/>"

        ?>
            <div class="item1">

                <?php
                $k = 0;
                while(file_exists("banner/astro[$k].txt")) {
                    $banTxt = fopen("banner/astro[$k].txt", 'r') or die("couldn't open file");
                    echo "<div class=\"mySlides fade\">";
                    echo "<a class=\"ttl\">".fgets($banTxt)."</a>";
                    echo "<a class=\"txt\">".fgets($banTxt)."</a>";
                    echo "<img src=\"banner/astro[$k].jpg\" height=\"350\" width=\"100%\">";
                    echo "</div>";
                    $k++;
                    fclose($artTxt);

                }
                ?>
<!--                    <div class="mySlides fade">-->
<!--                        <a class="ttl">First man on the Moon</a>-->
<!--                        <a class="txt">Was he even there? Or Not?</a>-->
<!--                        <img src="banner/astro.jpg" height="350">-->
<!--                    </div>-->
<!--                    -->
<!--                    <div class="mySlides fade">-->
<!--                        <a class="ttl">Forest</a>-->
<!--                        <a class="txt">And a little hut in the front</a>-->
<!--                        <img src="banner/astro2.jpg" height="350">-->
<!--                    </div>-->
<!--                    -->
<!--                    <div class="mySlides fade">-->
<!--                        <a class="ttl">First man on the Moon</a>-->
<!--                        <a class="txt">Was he even there? Or Not?</a>-->
<!--                        <img src="banner/astro3.jpg" height="350">-->
<!--                    </div>-->
<!--                    -->
<!--                    <div class="mySlides fade">-->
<!--                        <a class="ttl">First man on the Moon</a>-->
<!--                        <a class="txt">Was he even there? Or Not?</a>-->
<!--                        <img src="banner/astro4.jpg" height="350">-->
<!--                    </div>-->

            </div>
            
            <div class="item2">
                <img src="images/rakete1.jpg" width="400">
                <div class="overlay">Colorfull Rocket</div>
            </div>
            
            <div class="txtimg">
                <img src="article/image.jpg" height="400">

                <?php
                $artTxt = fopen('article/text.txt', 'r');
                echo "<headLine>".fgets($artTxt)."</headLine>";
                echo "<lines>";
                while(false == feof($artTxt)) {
                    echo fgets($artTxt)."<br>";
                }
                fclose($artTxt);
                ?></lines>
            </div>
            <div class="item5"><z>There could be an Article or News or something</z></div>
            <div class="item6"><video controls width=100%>
                <source src="earth.mp4" type="video/mp4" /></video></div>
            <div class="item7"><img src="images/science.jpg"  height="100%"></div>
            <div class="item8"><img src="images/bird.jpg" height="100%"></div>
            <div class="item10"><img src="images/nudles.jpg" width="400"></div>
        </div>
        
    <script>
        var myIndex = 0;
        carousel();

        function carousel() {
            var i;
            var x = document.getElementsByClassName("mySlides");
            for (i = 0; i < x.length; i++) {
                x[i].style.display = "none";  
            }
            
            myIndex++;
            
            if (myIndex > x.length) {myIndex = 1}    
            x[myIndex-1].style.display = "block";  
            setTimeout(carousel, 2500);
        }
    </script>
    </body>

</html>
