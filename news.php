<!DOCTYPE html>
<html>
<title>NASA</title>
<link rel="stylesheet" type="text/css" href="nasa_style.css">
<link rel="stylesheet" href"https://www.w3schools.com/w3css/4/w3.css">

<style>
    .mySlides{display:none;}
    .fade {
        -webkit-animation-name: fade;
        -webkit-animation-duration: 1.5s;
        animation-name: fade;
        animation-duration: 1.5s;
    }

    @-webkit-keyframes fade {
        from {width: 0}
        to {width: 100%}
    }

    @keyframes fade {
        from {width: 0}
        to {width: 100%}
    }

</style>
    <body>

        <div class="navbar">
            <a class="navbar_logo" href="index.php"></a>
            <div class="search">
                <input type="text" placeholder="Search" name="search">
                <button type="submit">Q</button>
            </div>
            <div class="login">
                <form action="admin.php">
                    <input type="submit" value="login">
                </form>
            </div>
            <div class="main_menu">
                <li>
                    <div class="dropdown">
                        <a class="dropbtn">Missions</a>
                        <div class="dropdown-content">
                            <a> adf </a>
                            <a> adf </a>
                            <a> adf </a>
                            <a> adf </a>
                        </div>
                    </div>
                </li>

                <li>
                    <div class="dropdown">
                        <a class="dropbtn">Galeries</a>
                        <div class="dropdown-content">
                            <a> adf </a>
                            <a> adf </a>
                            <a> adf </a>
                            <a> adf </a>
                        </div>
                    </div>
                </li>

                <li>
                    <div class="dropdown">
                        <a class="dropbtn">NASA TV</a>
                        <div class="dropdown-content">
                            <a> adf </a>
                            <a> adf </a>
                            <a> adf </a>
                            <a> adf </a>
                        </div>
                    </div>
                </li>

                <li>
                    <div class="dropdown">
                        <a class="dropbtn">Follow NASA</a>
                        <div class="dropdown-content">
                            <a> adf </a>
                            <a> adf </a>
                            <a> adf </a>
                            <a> adf </a>
                        </div>
                    </div>
                </li>

                <li>
                    <div class="dropdown">
                        <a class="dropbtn">Downloads</a>
                        <div class="dropdown-content">
                            <a> adf </a>
                            <a> adf </a>
                            <a> adf </a>
                            <a> adf </a>
                        </div>
                    </div>
                </li>

                <li>
                    <div class="dropdown">
                        <a class="dropbtn">About</a>
                        <div class="dropdown-content">
                            <a> adf </a>
                            <a> adf </a>
                            <a> adf </a>
                            <a> adf </a>
                        </div>
                    </div>
                </li>

                <li>
                    <div class="dropdown">
                        <a class="dropbtn">NASA Audiences</a>
                        <div class="dropdown-content">
                            <a> adf </a>
                            <a> adf </a>
                            <a> adf </a>
                            <a> adf </a>
                        </div>
                    </div>
                </li>

            </div>
            <div class="sub_menu">
                <li><a>Humans in Space</a></li>
                <li><a>Moon to Mars</a></li>
                <li><a>Earth</a></li>
                <li><a>Space Tech</a></li>
                <li><a>Flight</a></li>
                <li><a>Solar System and beyond</a></li>
                <li><a>Education</a></li>
                <li><a>History</a></li>
                <li><a>Benifits to you</a></li>
            </div>
        </div>
    </body>
</html>