<?php
if(isset($_POST['imgUpload'])) {


//    Move old article to archive so nothing is lost
    moveToArchive();
    $text =$_POST['titleForImage'];
    $artTxt = fopen('article/text.txt', 'w') or die(print_r(error_get_last(),true));
    fwrite($artTxt,$text."\n");
    $text = $_POST['textForImage'];
    fwrite($artTxt,$text);
    fclose($artTxt);


//    set image Parameters
    $file = $_FILES['fileForIMAGE'];
    print_r($file);
    $fileName = $_FILES['fileForIMAGE']['name'];
    $fileTmpName = $_FILES['fileForIMAGE']['tmp_name'];
    $fileSize = $_FILES['fileForIMAGE']['size'];
    $fileError = $_FILES['fileForIMAGE']['error'];
    $fileType = $_FILES['fileForIMAGE']['type'];

    $fileExt = explode('.', $fileName);
    $fileActualExt = strtolower(end($fileExt));

    $allowed = array('jpg', 'jpeg');


//  Upload image
    if (in_array($fileActualExt, $allowed)) {
        if ($fileError === 0) {
            if ($fileSize < 1000000) {
                $fileNameNew = "image." . $fileActualExt;
                $fileDestination = 'article/' . $fileNameNew;
                move_uploaded_file($fileTmpName, $fileDestination);
                header("Location: admin.php?uploadsuccess");
            } else {
                echo "Your file is to big!";
            }
        } else {
            echo "There has been an error";
        }
    } else {
        echo "You can only Upload jpg or png";
    }
}

if(isset($_POST['bannerUpload'])) {

    $k = 0;
    $result="";
    while(!$result){
        if(!file_exists("banner/astro[$k].txt")) {
            $result = "astro[$k]";
        }
        $k++;
    }

    $newFile = fopen("banner/$result.txt", "w");
    $text =$_POST['titleForBanner'];
    fwrite($newFile,$text."\n");
    $text = $_POST['textForBanner'];
    fwrite($newFile,$text);
    fclose($newFile);



    $file = $_FILES['fileForBanner'];
    print_r($file);
    $fileName = $_FILES['fileForBanner']['name'];
    $fileTmpName = $_FILES['fileForBanner']['tmp_name'];
    $fileSize = $_FILES['fileForBanner']['size'];
    $fileError = $_FILES['fileForBanner']['error'];
    $fileType = $_FILES['fileForBanner']['type'];

    $fileExt = explode('.', $fileName);
    $fileActualExt = strtolower(end($fileExt));

    $allowed = array('jpg', 'jpeg', 'png');

    if (in_array($fileActualExt, $allowed)) {
        if ($fileError === 0) {
            if ($fileSize < 1000000) {
                $fileNameNew = $result. "." . $fileActualExt;
                $fileDestination = 'banner/' . $fileNameNew;
                move_uploaded_file($fileTmpName, $fileDestination);
                header("Location: admin.php?uploadsuccess");
            } else {
                echo "Your file is to big!";
            }
        } else {
            echo "There has been an error";
        }
    } else {
        echo "You can only Upload jpg or png";
    }
}


function moveToArchive(){
    $k = 0;
    $result="";
    while(!$result){
        if(!file_exists("archive/article[$k].txt")) {
            $result = "article[$k]";
        }
        $k++;
    }
    rename("article/text.txt", "archive/".$result.".txt");
    rename("article/image.jpg", "archive/".$result.".jpg");
}
?>
